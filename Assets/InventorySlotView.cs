using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Inventory
{
    public class InventorySlotView : MonoBehaviour
    {
        [SerializeField] public TextMeshProUGUI Name;
        [SerializeField] public TextMeshProUGUI Amount;
        [SerializeField] public GameObject AmountContainer;
        [SerializeField] public Image Sprite;
        public Action<Item> Action;
        public Action<Recipe> ActionRecipe;
        public Item Item;
        public Recipe Recipe;
        
        public void ButtonAction()
        {
            Action?.Invoke(Item);
            ActionRecipe?.Invoke(Recipe);
        }
    }
}
