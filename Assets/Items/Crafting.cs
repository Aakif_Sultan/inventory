using UnityEngine;

namespace Inventory
{
    [CreateAssetMenu (fileName = "Crafting", menuName = "items/Crafting")]
    public class Crafting : Item
    {
        public CraftingType CraftingType;
        
        public Crafting(string id, string name, string rarity, float weight, float cost, Sprite icon, CraftingType craftingType)
        {
            Id = id;
            Name = name;
            Rarity = rarity;
            Weight = weight;
            Cost = cost;
            Icon = icon;
            CraftingType = craftingType;
            Type = ItemType.Crafting;
        }
    }

    public enum CraftingType
    {
        Weapon,
        Armor,
        Consumable
    }
}