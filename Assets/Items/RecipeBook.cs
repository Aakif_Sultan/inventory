using System.Collections.Generic;
using UnityEngine;

namespace Inventory
{
    [CreateAssetMenu (fileName = "RecipeBook", menuName = "RecipeBook")]
    public class RecipeBook : ScriptableObject
    {
        public List<Recipe> Recipes;
    }
}