using UnityEngine;

namespace Inventory
{
    [CreateAssetMenu (fileName = "QuestItem", menuName = "items/QuestItem")]
    public class QuestItem : Item
    {
        public string QuestId;
        
        public QuestItem(string id, string name, string rarity, float weight, float cost, Sprite icon, string questId)
        {
            Id = id;
            Name = name;
            Rarity = rarity;
            Weight = weight;
            Cost = cost;
            Icon = icon;
            QuestId = questId;
            Type = ItemType.Quest;
        }
    }
}