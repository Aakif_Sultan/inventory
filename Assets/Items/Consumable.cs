using UnityEngine;

namespace Inventory
{
    [CreateAssetMenu (fileName = "Consumable", menuName = "items/Consumable")]
    public class Consumable : Item
    {
        public float Amount;
        public float Duration;
        public ConsumableEffect Effect;
        
        public Consumable(string id, string name, string rarity, float weight, float cost, Sprite icon, 
            float amount, float duration, ConsumableEffect effect)
        {
            Id = id;
            Name = name;
            Rarity = rarity;
            Weight = weight;
            Cost = cost;
            Icon = icon;
            Amount = amount;
            Duration = duration;
            Effect = effect;
            Type = ItemType.Consumable;
        }
    }

    public enum ConsumableEffect
    {
        Healing,
        DamageIncrease,
        DamageReductionIncrease,
        SpeedIncrease
    }
}