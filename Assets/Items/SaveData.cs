using System.Collections.Generic;

namespace Inventory
{
    public class SaveData
    {
        public List<Item> InventoryItems = new List<Item>();
        public List<Item> EquippedItems = new List<Item>();
        public int SortState;
    }
}
