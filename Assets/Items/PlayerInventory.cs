using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Inventory
{
    [CreateAssetMenu(fileName = "PlayerInventory", menuName = "PlayerInventory")]
    public class PlayerInventory : ScriptableObject
    {
        public List<InventorySlot> InventorySlot = new List<InventorySlot>(); // LIST OF ITEMS WITH AMOUNTS
        public List<Item> InventoryItems = new List<Item>();
        public List<Item> EquippedItems = new List<Item>();

        public void AddItemInitiate(Item item)
        {
            bool itemExists = false;

            for (int i = 0; i < InventorySlot.Count; i++)
            {
                if (InventorySlot[i].Item.Id == item.Id)
                {
                    InventorySlot[i].Add();
                    itemExists = true;
                }
            }

            if (!itemExists)
            {
                InventorySlot slot = new InventorySlot(item, 1);
                InventorySlot.Add(slot);
            }
        }
        
        private void AddItem(Item item)
        {
            bool itemExists = false;

            for (int i = 0; i < InventorySlot.Count; i++)
            {
                if (InventorySlot[i].Item.Id == item.Id)
                {
                    InventorySlot[i].Add();
                    itemExists = true;
                    InventoryItems.Add(item);
                }
            }

            if (!itemExists)
            {
                InventorySlot slot = new InventorySlot(item, 1);
                InventorySlot.Add(slot);
                InventoryItems.Add(item);
            }
        }

        private bool RemoveItem(Item item) // returns whether player had the item or not
        {
            for (int i = 0; i < InventorySlot.Count; i++)
            {
                if (InventorySlot[i].Item.Id == item.Id)
                {
                    InventorySlot[i].Remove();

                    if (InventorySlot[i].Amount < 1)
                    {
                        InventorySlot.RemoveAt(i);
                    }

                    InventoryItems.Remove(item);
                    
                    return true;
                }
            }

            return false;
        }

        public bool ConsumeItem(Item item, Action playerConsumeAction) // Use consumable item.
        {
            if (InventoryItems.Contains(item) && item.Type == ItemType.Consumable)
            {
                playerConsumeAction.Invoke();
                return RemoveItem(item);
            }

            return false;
        }
        
        public void EquipItem(Item item) // Equip Weapon or Armor
        {
            Debug.Log($"EquipItem {item.name}");
            bool hasWeapon = false;
            bool hasArmor = false;
            
            for (int i = 0; i < EquippedItems.Count; i++)
            {
                if (EquippedItems[i].Type == ItemType.Weapon)
                {
                    hasWeapon = true;
                }
                else if (EquippedItems[i].Type == ItemType.Armor)
                {
                    hasArmor = true;
                }
            }

            if (InventoryItems.Contains(item) &&
                ((item.Type == ItemType.Weapon && !hasWeapon) || (item.Type == ItemType.Armor && !hasArmor)))
            {
                EquippedItems.Add(item);
                RemoveItem(item);
            }
        }

        public void UnEquipItem(Item item) // Unequip Armor
        {
            Debug.Log($"UnEquipItem {item.name}");
            if (EquippedItems.Contains(item) &&
                item.Type is ItemType.Weapon or ItemType.Armor)
            {
                EquippedItems.Remove(item);
                AddItem(item);
            }
        }

        public void AddRecipe(Recipe recipe)
        {
            List<Item> ingredientsToUse = new List<Item>();
            
            for (int i = 0; i < recipe.Components.Count; i++)
            {
                foreach (var item in InventoryItems)
                {
                    if (item.Id == recipe.Components[i].Id)
                    {
                        ingredientsToUse.Add(item);
                        RemoveItem(item);
                        break;
                    }
                }
            }

            if (ingredientsToUse.Count == recipe.Components.Count)
            {
                InventoryItems.Add(recipe.Result);
                Debug.Log($"Created {recipe.Result.name}");
            }
            else
            {
                for (int i = 0; i < ingredientsToUse.Count; i++)
                {
                    AddItem(ingredientsToUse[i]);
                }
                
                Debug.Log($"Need more craftables to make {recipe.Result.name}");
            }
        }

        public void SortById()
        {
            var sortedList = InventorySlot.OrderBy(slot => slot.Item.Id).ToList();
            InventorySlot = sortedList;
        }
        
        public void SortByIdAlt()
        {
            var sortedList = InventorySlot.OrderByDescending(slot => slot.Item.Id).ToList();
            InventorySlot = sortedList;
        }

        public void SortByWeight()
        {
            var sortedList = InventorySlot.OrderBy(slot => slot.Item.Weight).ToList();
            InventorySlot = sortedList;
        }
        
        public void SortByWeightAlt()
        {
            var sortedList = InventorySlot.OrderByDescending(slot => slot.Item.Weight).ToList();
            InventorySlot = sortedList;
        }

        public void SortByCost()
        {
            var sortedList = InventorySlot.OrderBy(slot => slot.Item.Cost).ToList();
            InventorySlot = sortedList;
        }
        
        public void SortByCostAlt()
        {
            var sortedList = InventorySlot.OrderByDescending(slot => slot.Item.Cost).ToList();
            InventorySlot = sortedList;
        }

        public void SortByName()
        {
            var sortedList = InventorySlot.OrderBy(slot => slot.Item.Name).ToList();
            InventorySlot = sortedList;
        }
        
        public void SortByNameAlt()
        {
            var sortedList = InventorySlot.OrderByDescending(slot => slot.Item.Name).ToList();
            InventorySlot = sortedList;
        }
        
        public void SortByType()
        {
            var sortedList = InventorySlot.OrderBy(slot => slot.Item.Type).ToList();
            InventorySlot = sortedList;
        }
        
        public void SortByTypeAlt()
        {
            var sortedList = InventorySlot.OrderByDescending(slot => slot.Item.Type).ToList();
            InventorySlot = sortedList;
        }
    }

    [System.Serializable]
    public class InventorySlot
    {
        public Item Item;
        public int Amount;

        public InventorySlot(Item item, int amount)
        {
            Item = item;
            Amount = amount;
        }

        public void Add()
        {
            Amount++;
        }

        public void Remove()
        {
            Amount--;
        }
    }
}
