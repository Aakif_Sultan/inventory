using UnityEngine;

namespace Inventory
{
    [CreateAssetMenu (fileName = "Item", menuName = "items/New Item")]
    public class Item : ScriptableObject
    {
        public string Id;
        public string Name;
        public string Description;
        public string Rarity;
        public float Weight;
        public float Cost;
        public Sprite Icon;
        public ItemType Type;
    }

    public enum ItemType
    {
        Weapon,
        Armor,
        Consumable,
        Crafting,
        Quest
    }
}
