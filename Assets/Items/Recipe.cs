using System.Collections.Generic;
using UnityEngine;

namespace Inventory
{
    [CreateAssetMenu (fileName = "Recipe", menuName = "Recipe/New Item")]
    public class Recipe : ScriptableObject
    {
        public List<Item> Components;
        public Item Result;
    }
}
