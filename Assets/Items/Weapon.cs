using UnityEngine;

namespace Inventory
{
    [CreateAssetMenu (fileName = "Weapon", menuName = "items/Weapon")]
    public class Weapon : Item
    {
        public float Damage;
        public float Speed;
        public float Range;
        
        public Weapon(string id, string name, string rarity, float weight, float cost, Sprite icon, float damage, 
            float speed, float range)
        {
            Id = id;
            Name = name;
            Rarity = rarity;
            Weight = weight;
            Cost = cost;
            Icon = icon;
            Damage = damage;
            Speed = speed;
            Range = range;
            Type = ItemType.Weapon;
        }
    }
}
