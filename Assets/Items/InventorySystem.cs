using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Inventory
{
    public class InventorySystem : MonoBehaviour
    {
        [SerializeField] private PlayerInventory _inventory;
        [SerializeField] private GameObject _inventoryContainer;
        [SerializeField] private GameObject _equippedContainer;
        [SerializeField] private GameObject _recipeContainer;
        [SerializeField] private GameObject _inventorySlotPrefab;
        [SerializeField] private RecipeBook _recipeBook;
        [SerializeField] private ItemLibrary _itemLibrary;
        [SerializeField] private TMP_Dropdown _dropdown;
        [SerializeField] private Toggle _weaponToggle;
        [SerializeField] private Toggle _armorToggle;
        [SerializeField] private Toggle _consumableToggle;
        [SerializeField] private Toggle _craftingToggle;
        [SerializeField] private Toggle _questItemToggle;

        private List<InventorySlotView> _equippedItems;
        private List<InventorySlotView> _inventoryItems;
        private List<InventorySlotView> _recipesItems;
        private SaveData _saveData;
        private string _saveDirectory;
        private string _saveFile;
        
        private void Awake()
        {
            _saveData = new SaveData();
            _saveDirectory = Application.persistentDataPath + "/SaveData";
            _saveFile = _saveDirectory + "/Save";

            if (_inventory.InventorySlot != null)
            {
                _inventory.InventorySlot.Clear();
            }

            //LoadInventory();

            _weaponToggle.onValueChanged.AddListener(RefreshInventorySlots);
            _armorToggle.onValueChanged.AddListener(RefreshInventorySlots);
            _consumableToggle.onValueChanged.AddListener(RefreshInventorySlots);
            _craftingToggle.onValueChanged.AddListener(RefreshInventorySlots);
            _questItemToggle.onValueChanged.AddListener(RefreshInventorySlots);

            PopulateSomeData(); // uncomment once to populate inventory
        }

        private void PopulateSomeData() // to create an example inventory
        {
            _inventory.InventoryItems = new List<Item>();
            _inventory.InventoryItems.Add(_itemLibrary.Items[0]);
            _inventory.InventoryItems.Add(_itemLibrary.Items[0]);
            _inventory.InventoryItems.Add(_itemLibrary.Items[1]);
            _inventory.InventoryItems.Add(_itemLibrary.Items[1]);
            _inventory.InventoryItems.Add(_itemLibrary.Items[1]);
            _inventory.InventoryItems.Add(_itemLibrary.Items[2]);
            _inventory.InventoryItems.Add(_itemLibrary.Items[2]);
            _inventory.InventoryItems.Add(_itemLibrary.Items[3]);
            _inventory.InventoryItems.Add(_itemLibrary.Items[3]);
            _inventory.InventoryItems.Add(_itemLibrary.Items[3]);
            _inventory.InventoryItems.Add(_itemLibrary.Items[4]);
            _inventory.InventoryItems.Add(_itemLibrary.Items[4]);
            _inventory.InventoryItems.Add(_itemLibrary.Items[5]);
            _inventory.InventoryItems.Add(_itemLibrary.Items[5]);
            
            _inventory.EquippedItems = new List<Item>();
            _inventory.EquippedItems.Add(_itemLibrary.Items[2]);
            _inventory.EquippedItems.Add(_itemLibrary.Items[5]);
        }

        public void SaveInventory()
        {
            if(!Directory.Exists(_saveDirectory))
            {
                Directory.CreateDirectory(_saveDirectory);
            }

            _saveData.InventoryItems = _inventory.InventoryItems;
            _saveData.SortState = _dropdown.value;
            _saveData.EquippedItems = _inventory.EquippedItems;
            string json = JsonUtility.ToJson(_saveData);
            File.WriteAllText(_saveFile, json);
            
            RefreshInventorySlots();
        }

        public void LoadInventory()
        {
            if (File.Exists(_saveFile))
            {
                string json = File.ReadAllText(_saveFile);
                _saveData = JsonUtility.FromJson<SaveData>(json);
                _inventory.InventoryItems = _saveData.InventoryItems;
                _dropdown.value = _saveData.SortState;
                _inventory.EquippedItems = _saveData.EquippedItems;
                RefreshInventorySlots();
            }
            else
            {
                Debug.Log("File Doesn't Exist");
            }
        }

        private void ClearInventoryItems()
        {
            if (_inventoryItems != null)
            {
                foreach (var items in _inventoryItems)
                {
                    Destroy(items.gameObject);
                }

                _inventoryItems.Clear();
            }
            
            _inventoryItems = new List<InventorySlotView>();
            
            if (_equippedItems != null)
            {
                foreach (var items in _equippedItems)
                {
                    Destroy(items.gameObject);
                }

                _equippedItems.Clear();
            }
            
            _equippedItems = new List<InventorySlotView>();
            
            if (_recipesItems != null)
            {
                foreach (var items in _recipesItems)
                {
                    Destroy(items.gameObject);
                }

                _recipesItems.Clear();
            }
            
            _recipesItems = new List<InventorySlotView>();
        }

        public void RefreshInventorySlots(bool toggle = true)
        {
            _inventory.InventorySlot.Clear();
            
            foreach (Item item in _saveData.InventoryItems)
            {
                _inventory.AddItemInitiate(item);
            }
            
            CreateInventoryItems();
            SortBy();
        }

        public void CreateInventoryItems()
        {
            ClearInventoryItems();
            
            int counter = 0;
            
            for (int i = 0; i < _inventory.InventorySlot.Count; i++)
            {
                if ((_weaponToggle.isOn && _inventory.InventorySlot[i].Item.Type == ItemType.Weapon)
                    || (_armorToggle.isOn && _inventory.InventorySlot[i].Item.Type == ItemType.Armor)
                    || (_consumableToggle.isOn && _inventory.InventorySlot[i].Item.Type == ItemType.Consumable)
                    || (_craftingToggle.isOn && _inventory.InventorySlot[i].Item.Type == ItemType.Crafting)
                    || (_questItemToggle.isOn && _inventory.InventorySlot[i].Item.Type == ItemType.Quest))
                {
                    _inventoryItems.Add(Instantiate(_inventorySlotPrefab, _inventoryContainer.transform).GetComponent<InventorySlotView>());
                    _inventoryItems[counter].Sprite.sprite = _inventory.InventorySlot[i].Item.Icon;
                    _inventoryItems[counter].Amount.text = ($"{_inventory.InventorySlot[i].Amount}");
                    _inventoryItems[counter].Name.text = ($"{_inventory.InventorySlot[i].Item.Name}");
                    _inventoryItems[counter].AmountContainer.SetActive(_inventory.InventorySlot[i].Amount > 1);
                    _inventoryItems[counter].Action = Equip;
                    _inventoryItems[counter].Item = _inventory.InventorySlot[i].Item;
                    counter++;
                }
            }
            
            int counterEquipped = 0;
            
            for (int i = 0; i < _inventory.EquippedItems.Count; i++)
            {
                _equippedItems.Add(Instantiate(_inventorySlotPrefab, _equippedContainer.transform).GetComponent<InventorySlotView>());
                _equippedItems[counterEquipped].Sprite.sprite = _inventory.EquippedItems[i].Icon;
                _equippedItems[counterEquipped].Name.text = ($"{_inventory.EquippedItems[i].Name}");
                _equippedItems[counterEquipped].AmountContainer.SetActive(false);
                _equippedItems[counterEquipped].Action = UnEquip;
                _equippedItems[counterEquipped].Item = _inventory.EquippedItems[i];

                counterEquipped++;
            }
            
            int counterRecipes = 0;
            
            for (int i = 0; i < _recipeBook.Recipes.Count; i++)
            {
                _recipesItems.Add(Instantiate(_inventorySlotPrefab, _recipeContainer.transform).GetComponent<InventorySlotView>());
                _recipesItems[counterRecipes].Sprite.sprite = _recipeBook.Recipes[i].Result.Icon;
                _recipesItems[counterRecipes].Name.text = ($"{_recipeBook.Recipes[i].Result.name}");
                _recipesItems[counterRecipes].AmountContainer.SetActive(false);
                _recipesItems[counterRecipes].ActionRecipe = MakeRecipe;
                _recipesItems[counterRecipes].Recipe = _recipeBook.Recipes[i];

                counterRecipes++;
            }
        }

        private void Equip(Item item)
        {
            _inventory.EquipItem(item);
            RefreshInventorySlots();
        }

        private void UnEquip(Item item)
        {
            _inventory.UnEquipItem(item);
            RefreshInventorySlots();
        }
        
        private void MakeRecipe(Recipe recipe)
        {
            _inventory.AddRecipe(recipe);
            RefreshInventorySlots();
        }
        
        public void Sort()
        {
            SortBy();
            CreateInventoryItems();
        }

        void SortBy()
        {
            switch(_dropdown.value)
            {
                case 0:
                    _inventory.SortById();
                    break;
                case 1:
                    _inventory.SortByIdAlt();
                    break;
                case 2:
                    _inventory.SortByWeight();
                    break;
                case 3:
                    _inventory.SortByWeightAlt();
                    break;
                case 4:
                    _inventory.SortByCost();
                    break;
                case 5:
                    _inventory.SortByCostAlt();
                    break;
                case 6:
                    _inventory.SortByName();
                    break;
                case 7:
                    _inventory.SortByNameAlt();
                    break;
                case 8:
                    _inventory.SortByType();
                    break;
                case 9:
                    _inventory.SortByTypeAlt();
                    break;
            }

            _saveData.SortState = _dropdown.value;
        }
    }
}
