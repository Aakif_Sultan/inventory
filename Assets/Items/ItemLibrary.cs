using System.Collections.Generic;
using UnityEngine;

namespace Inventory
{
    [CreateAssetMenu (fileName = "ItemLibrary", menuName = "ItemLibrary")]
    public class ItemLibrary : ScriptableObject
    {
        public List<Item> Items;
    }
}