using UnityEngine;

namespace Inventory
{
    [CreateAssetMenu (fileName = "Armor", menuName = "items/Armor")]
    public class Armor : Item
    {
        public float DamageReduction;
        public float SpeedModifier;
        
        public Armor(string id, string name, string rarity, float weight, float cost, Sprite icon, 
            float damageReduction, float speedModifier)
        {
            Id = id;
            Name = name;
            Rarity = rarity;
            Weight = weight;
            Cost = cost;
            Icon = icon;
            DamageReduction = damageReduction;
            SpeedModifier = speedModifier;
            Type = ItemType.Armor;
        }
    }
}
